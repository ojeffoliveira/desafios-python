# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

def contarBombas(array, linha, coluna):
    if (array[linha][coluna]=="*"):
        return "*"
    
    linhas  = len(array)
    colunas = len(array[0])
    
    numeroBombas =  0
    linhaDesvio  = -1
 
    while (linhaDesvio<=1):
        colunaDesvio = -1
        while (colunaDesvio<=1): 
            if( 0<=linha+linhaDesvio<linhas 
               and 0<=coluna+colunaDesvio<colunas):
                if(array[(linha+linhaDesvio)][(coluna+colunaDesvio)]=="*"):
                    numeroBombas+=1
            colunaDesvio=colunaDesvio+1
        linhaDesvio=linhaDesvio+1
        
    return str(numeroBombas)

def main():
    
    fields=[]
    i=0
    j=0
    
    while True:
        """ Define numero de linhas e colunas"""
        
        n = int(input("a"))
        m = int(input())
        
        """ Checa se foi passado 0 e 0"""
        if(n!=0 and m!=0):
            
            """ Contador de linhas"""
            i=0
            linhas=[]
            while i<n:
                linhas.append(input())
                i+=1
            
            """ Passa o field completo para
            o array de fields"""
            fields.append(linhas)
        else:
            break
  
    
    limite = len(fields)
    fieldCont=0
    while fieldCont<limite:
        n=len(fields[fieldCont])
        m=len(fields[fieldCont][0])
        i=0
       
        while i<n:
            aux=""
            j=0
            while j<m:
                aux=aux+contarBombas(fields[fieldCont],i,j)
                j=j+1
            fields[fieldCont][i]=aux
            i=i+1
        fieldCont+=1
    
    fieldCont=0
    while fieldCont<limite:
        n=len(fields[fieldCont])
        
        i=0
        
        print("Field #" + str(fieldCont+1) +":")
        while i<n:
            print(fields[fieldCont][i])
            i=i+1
        print("\n")
        fieldCont+=1
            
main()
            

            








    
