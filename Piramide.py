# -*- coding: utf-8 -*-
"""
Created on Fri May  3 20:46:06 2019

@author: Jefferson Oliveira
"""

def main():
    n = int(input())
    """ matriz das caixas contendo seus pesos """
    matriz = []
    
    cont = 0
    while cont <n:
    #for j in range(n):
        linha = (input())
        tamanho = len(linha)
        aux = []
        prox_num = ""
        for i in range(tamanho+1):
            if i <tamanho and (not linha[i]==" "):
                prox_num += linha[i]
            else:
                aux.append(int(prox_num))
                prox_num = ""
            
        matriz.append(aux)
        cont+=1
    pira = produz_piramide(matriz)
    #for k in range(n):
        #print(pira[k])
        
    print(soma_peso(pira))
    
    
    
def produz_piramide(matriz):
    """lista -> lista 
    recebe uma matriz e produz 
    a pirâmide com o menor peso possível"""
    piramide = matriz
    n = len(piramide)
    
    """ esse for define a linha que 
    estamos construindo agora"""
    for i in range(n-1):
        escolha = []
        """ Define qual coluna estamos somando agora"""
        for k in range(n):
            soma = 0
            """ Soma as linhas da coluna que
            estamos somando"""
            for j in range(n-i):
                soma+=piramide[i+j][k]
            
            """ Coloca a soma dessa coluna na lista
            escolha"""
            escolha.append([soma,k])
        """ Define quem é maior e menor na lista escolha"""
        
        for k in range(n):
            menor = k
            for j in range(k,n):
                if escolha[k][0]>escolha[j][0]:
                    menor = j
            aux = escolha[k]
            escolha[k] = escolha[menor]
            escolha[menor] = aux
            
        """ Deleta as caixas nesse andar"""
        if i == 0:
            for j in range(1,n):
                piramide[i][escolha[j][1]] = 0
        else:
            j = 0
            colocada = False
            while j<n and (not colocada):
                if piramide[i-1][escolha[j][1]] == 0:
                     for k in range(0,n):
                         if (not k == escolha[j][1]) and piramide[i-1][k] == 0:
                             piramide[i][k] = 0
                     colocada = True
                    
                else:
                    j+=1
                    
    return piramide
    
def soma_peso(lista):
    """ lista - > int
    recebe uma lista e soma os valores nela"""
    tamanho = len(lista)
    soma = 0
    for i in range(tamanho):
        for j in range(tamanho):
            soma += lista[i][j]
    
    return soma
    


main()