# -*- coding: utf-8 -*-
"""
Created on Sat Mar 16 19:28:38 2019

@author: Lenovo
"""

import random

DEBUG = True # se True, pede para digitar a semente e mostra a senha
MAX_TENTATIVAS = 20 # número máximos de chutes permitidos
NUM_DIGITOS  = 5   # número de dígitos da senha
MENOR_VALOR = pow(10, NUM_DIGITOS-1)    # menor número sorteado
MAIOR_VALOR = pow(10, NUM_DIGITOS)      # limite superior para o maior número sorteado (mais 1)

#####################################################################
def sortear():
    while True:
            valor =  str(random.randrange(MENOR_VALOR, MAIOR_VALOR))
            i=0
            repetido=False
            while i<len(valor):
                j=0
                while j<len(valor):
                    if i!=j and valor[i]==valor[j]:
                        repetido=True
                        break
                    j+=1
                i+=1
                
            if not repetido:
                break

    return valor

def main():
    '''(None) -> None 

    Coloque sua solução individual abaixo, seguindo o enunciado 
    desse exercício na página da disciplina.

    Siga as instruções para entrega disponíveis em:
    https://paca.ime.usp.br/mod/page/view.php?id=42515
    
    '''

    print("Bem vindo ao MASTER BIME!!")
    # as linhas abaixo vão ajudar durante o desenvolvimento e depuração 
    # para deixar de executá-las, basta modificar a constante DEBUG para 
    # False. Mas lembre-se de deixá-la True antes de enviar esse EP.



    if DEBUG:
        semente = int(input("Digite o valor da semente: "))
        random.seed(semente)
        senha = sortear()
        print("Número sorteado: ", senha)
    else:
        senha = sortear()
       
            
        
    # escreva seu programa a seguir
    senha = str(senha)
    
    
    """ Conta quantos chutes já foram dados """
    chutesCont=0
    print("Número de chutes: " + str(MAX_TENTATIVAS))
    print("Número de digitos: " + str(NUM_DIGITOS))
    
    """ O LOOP PRINCIPAL, recebe os
        chutes e lida com eles"""
    while chutesCont<MAX_TENTATIVAS:
        
        """ LOOP para receber chute e garantir
        que tem o numero certo de digitos"""
        while True:
            chute = str(input("\nDigite o chute #" +str(chutesCont+1) +": ")) 
            
            """ Checa se é uma string vazi """
            if chute=="":
                continue
            
            aux   = int(chute)
            
            """ Checa se tem o numero de digitos 
            certo"""
            if len(chute) == NUM_DIGITOS:
                break
            else:
                print("Digite um número com "+ str(NUM_DIGITOS) + " digitos.")
        
        if chute == senha:
            print("Parabéns, você acertou!")
            break
        
        digCertos=0
        posCertas=0
        i=0
        
        """ Loop para, no caso de nao ser
        o numero exato, contar as partes
        corretas"""
        while i<NUM_DIGITOS:
            """ """
            if senha[i]==chute[i]:
                posCertas+=1
            j=0    
            while j<NUM_DIGITOS:
                repetido = False
                k=0
                """ Checa se o digito já 
                já esteve nesse chute"""
                while k < j:
                    if chute[k]==chute[j]:
                       repetido = True
                       break
                   
                    k += 1
                """ """   
                if repetido:
                    j+=1
                    continue
                    
                """ """
                if senha[i]==chute[j]:
                    digCertos+=1
                j+=1
            i+=1
        
        
        
        
        
        print("Digitos corretos: \t" + str(digCertos))
        print("Posicoes corretas: \t" + str(posCertas))
       
        chutesCont=chutesCont+1
    
        
    if chutesCont==MAX_TENTATIVAS:
        print("Ha ha, você perdeu!")
        
    print("Fim do jogo.")
    print(senha)
    input()
    
main()