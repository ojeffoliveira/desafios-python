# -*- coding: utf-8 -*-
"""
Created on Fri May  3 22:18:01 2019

@author: Jefferson Oliveira
"""

def main():
    l = int(input())
    c = int(input())
    
    if (l+c)%2==0:
        print(1)
    else:
        print(0)
    
main()