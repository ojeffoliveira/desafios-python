# -*- coding: utf-8 -*-
"""
Created on Tue May 14 22:32:38 2019

@author: Jefferson Oliveira
"""

def main():
    
    n = int(input())
    d = int(input())
    a = int(input())
    
    if d>=a:
        print(d-a)
    else:
        print(((n-a))+(d))