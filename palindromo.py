# -*- coding: utf-8 -*-
"""
Created on Fri May 17 23:27:17 2019

@author: Jefferson Oliveira
"""


def main():
    
    n =int(input())
    cadeia = input()
    
    print(resposta(cadeia))
    
    return 


def resposta(texto):
    """
    Encontra a resposta para o 
    minimo de partes em que todas 
    vão ser palindromas
    """
    n = len(texto)
    achei = False
    fins =[]
    
    if eh_palindromo(texto,0,n):
        achei = True
                
    num_sep = 0
    while (not achei) and num_sep<(n-1):
        num_sep+=1
        fins = [0]*num_sep
        
        ult_achei = True
        
        while (not achei) and ult_achei: 
            ult_achei = False
            p = num_sep-1
            while (not ult_achei) and p>=0:
                if(sum(fins)==0):
                    for m in range(num_sep):
                        fins[m]=m+1
                    ult_achei = True
                    
                elif p == num_sep-1:
                    if fins[p] < n-1:
                        fins[p]+=1
                        ult_achei = True
                elif fins[p] < (fins[p+1]-1):
                    fins[p] = fins[p]+1
                    for k in range(p+1,num_sep):  
                        fins[k] = fins[k-1]+1
                    ult_achei = True
                p-=1
            
            if ult_achei:
                
                j = 1
                todos = True
                if not eh_palindromo(texto, 0,fins[0]):
                        todos= False
                    
                while todos and j <num_sep:
                    if not eh_palindromo(texto,fins[j-1],fins[j]):
                        todos= False
                    j+=1
                
                if not eh_palindromo(texto,fins[num_sep-1],n):
                        todos = False
                    
                if todos:
                    achei = True
        
    #print(fins)
   
    return (len(fins)+1)

def separar(texto,fins):
    """
    string, lista - > lista[string]
    Separa o texto com os fins definidos na lista
    fins. Esses fins são abertos
    """
    retornar = []
    tam = len(fins)
    
    retornar.append(texto[0:fins[0]])
    
    for i in range(1,tam):
        retornar.append(texto[fins[i-1]:fins[i]])
        
    retornar.append(texto[fins[len(fins)-1]:len(texto)])
        
    return retornar

def eh_palindromo(texto,i,f):
    """
    checa se a substring texto é um palindromo
    """
    tam = (f-i)
    metade = (tam//2)
    palindromo = True
    j = 0
    
    while j<metade and palindromo:    
        if not(texto[i+j]==texto[(f-1)-j]):
            palindromo = False
        j +=1
        
        
    return palindromo 


def reverta(texto):
    """
    (string) -> string
    retorna a string recebida ao contrário
    """
    tam = len(texto)
    resposta = ""
    
    for i in range(tam):
        resposta +=texto[(tam-1)-i]
        
    return resposta


main()