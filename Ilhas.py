# -*- coding: utf-8 -*-
"""
Created on Mon May  6 01:00:42 2019

@author: Jefferson Oliveira
"""

def main():
    valores  = extrair_valores(input())
    n = valores[0]
    c = valores[1]
    grafo = []
    
    for i in range(c):
        lig = extrair_valores(input())
        grafo.append(lig[:])
        aux = lig[0]
        lig[0]=lig[1]
        lig[1]=aux
        grafo.append(lig[:])
    s = int(input())        

    grafo = sorted(grafo,key=lambda num: num[0])    
    
    resul=[]    

    for i in range(1,n+1):
        
        if not i==s:
            inserir_em_ordem(resul,contar_pings(grafo,i,i,s,[i]))
    
    print(int(resul[len(resul)-1])-int(resul[0]))
    
    
    
def extrair_valores(texto):
    tamanho = len(texto)
    valores = []
    prox_num = ""
    for i in range(tamanho+1):
        if i==tamanho or texto[i]==" ":
            valores.append(int(prox_num))
            prox_num = ""
        else:
            prox_num +=texto[i]
    
    return valores
    
def somar_caminho(grafo,caminho):
    
    tamanho = len(caminho)-1
    pontos = len(grafo)
    soma = 0
    for i in range(tamanho):
        j = 0
        achei = False
        while j<pontos and not achei:
            if grafo[j][0]==caminho[i] and grafo[j][1]==caminho[i+1]:
                soma+=int(grafo[j][2])
                achei = True
            j+=1
    
    
    return soma

def contar_pings(grafo, ini, meio,fim, caminho):
    """ 
    grafo, ponto inicial, ponto do meio no qual se 
    está agora, ponto final, lista de pontos pelo qual
    se passou, lista de pings possíveis
    lista[lista[,,,]], int e int"""
    pings=[]
    
    
    
    if meio==fim:
        inserir_em_ordem(pings, somar_caminho(grafo,caminho))
        return pings
    
    cont = 0
    tam = len(grafo)
    while cont<tam and grafo[cont][0]<=meio:
        
        if (not grafo[cont][0]<meio) and (not caminho.__contains__(grafo[cont][1])) :
            prox_caminho = caminho[:]
            prox_caminho.append(grafo[cont][1])
            novo = contar_pings(grafo, ini, grafo[cont][1],fim,prox_caminho[:])    
            
            tam_pings = len(novo)
            for k in range(tam_pings):
                inserir_em_ordem(pings,novo[k])
            
        cont+=1
    
    if meio == ini:
        return pings[0]
    else:
        return pings

    
def inserir_em_ordem(lista,valor):
    
    tamanho = len(lista)
    
    for i in range(tamanho+1):
        if i==tamanho or valor<lista[i]:
            lista.insert(i,valor)
            return
        
main()