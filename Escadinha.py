# -*- coding: utf-8 -*-
"""
Created on Fri May  3 19:15:56 2019

@author: Jefferson Oliveira
"""

def main():
    
    n = int(input())
    nums = (input())
    lista = []
    
    
    tamanho = len(nums)
       
    prox_num = ""
    for i in range(tamanho+1):
        
        if i <tamanho and (not nums[i] == " "):
            prox_num += nums[i]
        else:
            lista.append(int(prox_num))
            prox_num = ""
    
    
    escadas = resolver(lista)
    print(escadas)
    
    
def resolver(lista):
    """ lista[] -> int escadas
    conta o número de escadas em uma sequência de números"""
    escadas = 0
    
    if len(lista) > 0:
        escadas = 1
        if len(lista) > 2:
            tamanho = len(lista)
            passo = (lista[1]-lista[0])
            for i in range(1,(tamanho-1)):
                if not lista[i+1]-lista[i] == passo:
                    escadas +=1
                    passo = lista[i+1]-lista[i]
    return escadas
      

main()

